import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {SSinger, Json, Album} from './json';
import { BrowserRouter, Route, Switch } from 'react-router-dom';


const supportsHistory = 'pushState' in window.history;

class Root extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      loaded: false,
      data: []
    };
  }
  componentDidMount(){
    let requestUrl = "http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2";
    fetch(requestUrl).then(
      res => res.json()
    ).then(
        data => {
          console.log('RESPONSE:', data);
          this.setState({
            data:data,
            loaded: true
          })
          console.log(this.state);
        }
      )
  }
  render(){
    let {loaded, data} = this.state;
    if( loaded === true){
      return(
        <div>
          <Switch>
           <Route path="/" exact component={App} />
           <Route path="/singers" exact render={ (props) => {
             return(
               <Json {...props} array={data} />
             );
           }} />
           <Route path="/singer/:id" render={(props) => {
             return(
               <SSinger {...props} array={data} />
             );
           }}/>
           <Route path="/album/:name" render={(props) => {
             return(
               <Album {...props} array={data} />
               );
           }}/>
          </Switch>
        </div>
      );
    } else {
      return(
        <div>Data Loading</div>
      )
    }


  }
}


ReactDOM.render(
  <BrowserRouter basename="/" forceRefrech={!supportsHistory}>
    <div className="App">
      <Root />
    </div>
  </BrowserRouter>,
  document.getElementById('root'));
