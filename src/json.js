import React, { Component } from 'react';
import { NavLink, Link } from 'react-router-dom';
import './App.css';
import ReactStars from "./ratingStar";
export class Json extends Component {

  constructor( props ){
    super(props);
  }

  componentWillReceiveProps(nextProps){
    console.log('nextProps', nextProps);
  }

  render(){
    console.log('render', this.props);
    let { array } = this.props;
    return(
     <div className="conteiner">
       <h1>Chose your favourite singer</h1>
       {

          array.map( (item, key) => {
            let num = "/singer/"+item.index;
            return(
              <li className="listitem" key={key}>
                <NavLink to={num} exact activeClassName="active" >
                  <span>{item.name}</span>
                </NavLink>
              </li>
            );
          })
       }
     </div>
    );
  }
}

export const SSinger = (props) => {

  let { array, match } = props;
  let selectedSinger;
  array.map( item => {
    console.log("important!", item, Number(item.index) === Number(match.params.id),  Number(item.index), Number(match.params.id))
    if(  Number(item.index) === Number(match.params.id) ){
      console.log("TRIGERED");
      selectedSinger = item;
    }
  });

  let albumName = selectedSinger.album;

  console.log("selectedSinger!", selectedSinger);

  return (
    <div className="album_conteiner">
      <Link to="/">back to list</Link>

      <h1>{props.match.params.id}. {selectedSinger.name} {albumName.name}</h1>
      <div> Chose some Album </div>
      <ul>
      {
        albumName.length !== 0 ?
        albumName.map( (item, key) => {
        let nn = "/album/"+item.name;
        let allDuration=0;
          item.compositions.map(item =>{
            allDuration -= item.duration.split(":").join("");
          })
          let outputDuration = allDuration.toString().split("");
          outputDuration.splice(0, 1);
          outputDuration.splice(-2, 0, ":");
          let finalDuration = outputDuration.join("");

          return(
            <div key={key}>              
                <li className="listitem">
                  <NavLink to={nn} exact activeClassName="active">
                    {item.name} with {item.compositions.length} {item.compositions.duration}songs, duration: {finalDuration}
                  </NavLink>
                </li>
              <div className="about-album">
                <div className="about"><span> About {item.name} </span></div>
                  {
                    item.about
                  }
              </div>
            </div>
            );
        })
        :
        <div className="NotFind">Sorry! But now we don't have this singer songs</div>

      }
      </ul>
    </div>
  );
};

export const Album = (props) => {
  let { array, match } = props;
  let albums;
  let composition;
  let albumName;
  array.map( item => {
    albums = item.album;
    albums.map( item => {
      console.log("important22!", item, item.compositions, item.name, match.params.name)
      console.log("TRIGERED");
      if(match.params.name==item.name){
        composition = item.compositions;  
        albumName = item.name;
      }    
    });    
  });

 let alll = composition;
 return (
    <div className="songs_conteiner">     
      <ul>
      <h1> {albumName} </h1>     
      {
        alll.length !== 0 ?
        alll.map( (item, key) => {
          return(
            <div key={key}>  
                <li className="listitem">
                    <div className="l_item">
                    <div>
                    name: {item.name}
                    <ReactStars
                    count={5}
                    size={24}
                    color2={'#ffd700'} 
                    />                        
                    </div>
                    <div>time: {item.duration}</div>
                    </div>
     
                </li> 
                
            </div>
            );
        })
        :
        <div className="NotFind">Sorry! But now we don't have this singer songs</div>
      }
      </ul>
    </div>
  );
};

